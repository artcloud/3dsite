import * as THREE from 'three';

import Stats from 'three/addons/libs/stats.module.js'; //показывает производительность системы - frames per second

import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

import { Octree } from 'three/addons/math/Octree.js'; // Восьмеричные деревья используются для разделения трёхмерного пространства, рекурсивно разделяя его на восемь ячеек - что оптимизирует работу с коллизиями
import { OctreeHelper } from 'three/addons/helpers/OctreeHelper.js';

import { Capsule } from 'three/addons/math/Capsule.js';

import { GUI } from 'three/addons/libs/lil-gui.module.min.js';

const clock = new THREE.Clock();

const scene = new THREE.Scene();
scene.background = new THREE.Color( 0x88ccee ); //вынести в настройки
scene.fog = new THREE.Fog( 0xFCEFC1, 0, 50 ); // настройка тумана - вынести

const camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 1000 ); //камера от первого лица игрока будет, ее поворот управляется мышью, перемещение - кнопками
camera.rotation.order = 'YXZ';

/*
const fillLight1 = new THREE.HemisphereLight( 0x8dc1de, 0x00668d, 0.5 ); //HemisphereLight( skyColor : Integer, groundColor : Integer, intensity : Float ) - вынести
fillLight1.position.set( 2, 5, 1 );
//const helperHemisphereLight = new THREE.HemisphereLightHelper( fillLight1, 1 );
scene.add( fillLight1 );
//scene.add( helperHemisphereLight );
*/

const directionalLight = new THREE.DirectionalLight(  'white', 2 );
directionalLight.position.set( 0, 1, 0 );
directionalLight.castShadow = true;            //дорогая операция, стоит подумать над целесобразностью ее использования
directionalLight.shadow.camera.near = 0.01;    //настройка камеры для теней - с помощью ее строятся тени
directionalLight.shadow.camera.far = 500;
directionalLight.shadow.camera.right = 30;
directionalLight.shadow.camera.left = - 30;
directionalLight.shadow.camera.top	= 30;
directionalLight.shadow.camera.bottom = - 30;
directionalLight.shadow.mapSize.width = 1024;
directionalLight.shadow.mapSize.height = 1024;
directionalLight.shadow.radius = 1;
directionalLight.shadow.bias = - 0.00006;
//const helperDirectionalLightHelper = new THREE.DirectionalLightHelper( directionalLight, 1 );
//scene.add( helperDirectionalLightHelper );
scene.add( directionalLight );

//const ambientLight = new THREE.AmbientLight( 0x404040, 2);
//scene.add( ambientLight );


const d1 = new THREE.DirectionalLight(  0xFCEFC1, 1 );
d1.position.set( 0, 0.5, 0 );
d1.target.position.set(-1, 0.1, 1);
const h1 = new THREE.DirectionalLightHelper( d1, 1 );
//scene.add( h1 );
scene.add( d1 );

const d2 = new THREE.DirectionalLight( 0xFCEFC1, 1 );
d2.position.set( 0, 0.5, 0 );
d2.target.position.set(1, 0.1, -1);
const h2 = new THREE.DirectionalLightHelper( d2, 1 );
//scene.add( h2 );
scene.add( d2 );

const container = document.getElementById( 'container' );

const renderer = new THREE.WebGLRenderer( { antialias: true } );  //сглаживание  - да
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMap.enabled = true;            //надо включать чтобы отрисовывались тени
renderer.shadowMap.type = THREE.VSMShadowMap; //тип отрисовки теней
renderer.toneMapping = THREE.ACESFilmicToneMapping;
container.appendChild( renderer.domElement );


//окошко производительности
const stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '10px';
container.appendChild( stats.domElement );
//


const GRAVITY = 30;
const STEPS_PER_FRAME = 5; //количество дополнительных расчетов позиций при каждой перерисовке браузера



let worldOctree = new Octree();

const playerCollider = new Capsule( new THREE.Vector3( 0, 0.35, 0 ), new THREE.Vector3( 0, 1, 0 ), 0.35 ); //объект для расчета коллизий игрока

const playerVelocity = new THREE.Vector3();   //скорость игрока
const playerDirection = new THREE.Vector3();  //направление игрока

let playerOnFloor = false;


const keyStates = {}; //объект в котором лежат коды кнопок и их статус нажатия

const vector1 = new THREE.Vector3();
const vector2 = new THREE.Vector3();
const vector3 = new THREE.Vector3();

document.addEventListener( 'keydown', ( event ) => {
    
    keyStates[ event.code ] = true;

} );

document.addEventListener( 'keyup', ( event ) => {

    keyStates[ event.code ] = false;

} );

container.addEventListener( 'mousedown', () => {

    document.body.requestPointerLock(); //это браузерный API - Не ограничивается границами браузера или экрана, Скрывает курсор, Обеспечивает информацию об относительных перемещениях мыши

} );

document.addEventListener( 'mouseup', () => {

    if ( document.pointerLockElement !== null ) mouseUpFunc();

} );

document.body.addEventListener( 'mousemove', ( event ) => {

    if ( document.pointerLockElement === document.body ) {

        camera.rotation.y -= event.movementX / 500;
        camera.rotation.x -= event.movementY / 500;

    }

} );

window.addEventListener( 'resize', onWindowResize );

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function mouseUpFunc() {

    console.log('mouseUpFunc')

}

function playerCollisions() {

    const result = worldOctree.capsuleIntersect( playerCollider );

    playerOnFloor = false;

    if ( result ) {

        playerOnFloor = result.normal.y > 0;

        if ( ! playerOnFloor ) {

            playerVelocity.addScaledVector( result.normal, - result.normal.dot( playerVelocity ) );

        }

        playerCollider.translate( result.normal.multiplyScalar( result.depth ) );

    }

}

function updatePlayer( deltaTime ) {

    let damping = Math.exp( - 4 * deltaTime ) - 1;

    if ( ! playerOnFloor ) {

        playerVelocity.y -= GRAVITY * deltaTime;

        // small air resistance
        damping *= 0.1;

    }

    playerVelocity.addScaledVector( playerVelocity, damping );

    const deltaPosition = playerVelocity.clone().multiplyScalar( deltaTime );
    playerCollider.translate( deltaPosition );

    playerCollisions();

    camera.position.copy( playerCollider.end );

}


function getForwardVector() {

    camera.getWorldDirection( playerDirection );
    playerDirection.y = 0;
    playerDirection.normalize();

    return playerDirection;

}

function getSideVector() {

    camera.getWorldDirection( playerDirection );
    playerDirection.y = 0;
    playerDirection.normalize();
    playerDirection.cross( camera.up );

    return playerDirection;

}

function controls( deltaTime ) {

    // gives a bit of air control
    const speedDelta = deltaTime * ( playerOnFloor ? 10 : 8 );

    if ( keyStates[ 'KeyW' ] ) {

        playerVelocity.add( getForwardVector().multiplyScalar( speedDelta ) );

    }

    if ( keyStates[ 'KeyS' ] ) {

        playerVelocity.add( getForwardVector().multiplyScalar( - speedDelta ) );

    }

    if ( keyStates[ 'KeyA' ] ) {

        playerVelocity.add( getSideVector().multiplyScalar( - speedDelta ) );

    }

    if ( keyStates[ 'KeyD' ] ) {

        playerVelocity.add( getSideVector().multiplyScalar( speedDelta ) );

    }

    if ( playerOnFloor ) {

        if ( keyStates[ 'Space' ] ) {

            playerVelocity.y = 6;

        }

    }

}


//загрузка локации на сцену
const loader = new GLTFLoader().setPath( './models/gltf/' );

loader.load( 'gallerypacked2.glb', ( gltf ) => {

    scene.add( gltf.scene );

    gltf.scene.scale.set( 0.4, 0.4, 0.4 )

    worldOctree.fromGraphNode( scene ); //при последующем добавлении объектов на сцену надо заново вызывать формирование дерева

    gltf.scene.traverse( child => {

        if ( child.isMesh ) {

            child.castShadow = true;
            child.receiveShadow = true;

            if ( child.material.map ) {

                child.material.map.anisotropy = 4;

            }

        }

    } );

    const helper = new OctreeHelper( worldOctree );
    helper.visible = false;
    scene.add( helper );

    const gui = new GUI( { width: 200 } );
    gui.add( { debug: false }, 'debug' )
        .onChange( function ( value ) {

            helper.visible = value;

        } );

    animate();

} );

function teleportPlayerIfOob() {

    if ( camera.position.y <= - 25 ) {

        playerCollider.start.set( 0, 0.35, 0 );
        playerCollider.end.set( 0, 1, 0 );
        playerCollider.radius = 0.35;
        camera.position.copy( playerCollider.end );
        camera.rotation.set( 0, 0, 0 );

    }

}


function animate() {

    const deltaTime = Math.min( 0.05, clock.getDelta() ) / STEPS_PER_FRAME; //clock.getDelta() возвращает сколько секунд прошло после предыдущего вызова анимации, это время делится ещо на несколько шагов (STEPS_PER_FRAME)

    // we look for collisions in substeps to mitigate the risk of
    // an object traversing another too quickly for detection.

    for ( let i = 0; i < STEPS_PER_FRAME; i ++ ) {  //в каждом шаге перерисовки браузера вызвается дополнительно STEPS_PER_FRAME раз все расчеты позиций, чтобы снизить риск неправльного расчета коллизий на больших скоростях движения объектов

        controls( deltaTime );

        updatePlayer( deltaTime );

        teleportPlayerIfOob();

    }

    renderer.render( scene, camera );

    stats.update(); //frames per second stats

    requestAnimationFrame( animate );

}



//загрузка картинок
/*
const loader = new THREE.TextureLoader();

// load a resource
loader.load(
    'media/pine-tree.png',
    function ( texture ) {
        const material = new THREE.MeshBasicMaterial( {
            map: texture,
            side: THREE.DoubleSide,
            alphaTest:.5
        });
        const meshTexture = new THREE.Mesh(
            new THREE.PlaneGeometry(.235,.235),
            material
        );
        meshTexture.position.set(.62,1,-.37);
        meshTexture.rotation.set(0,1.95,0);
        meshTexture.scale.set(0,0,0);
        scene.add(meshTexture)
        parent.add(meshTexture)
        anime({targets:meshTexture.scale,x:[0,.7],y:[0,.7],z:[0,1],duration:600,easing:'linear'}) //библиотека анимации - плавное появление
    },
    undefined,
    function ( e ) {
        console.error( e );
    }
);*/